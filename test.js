'use strict';

Number.prototype.bin = function () {
  var sign = (this < 0 ? '-' : '');
  var result = Math.abs(this).toString(2);
  while (result.length < 32) {
    result = '0' + result;
  }
  return sign + result;
};

var BuzHash = require('./index');

let data = require('./testdata');
const crypto = require('crypto');

function test (buf) {
  let sha = crypto.createHash('sha256');
  let hash = new BuzHash(32);
  let hashes = [];
  let c = 0;
  for (let i = 0; i < buf.length; i++) {
    let state = hash.update(buf.readInt8(i));
    sha.update(buf.slice(i, i+1));
    if ((state & 0b111111111) === 0) {
      let shahash = sha.digest('base64');
      console.log(`${i}\t${shahash}`);
      hashes.push({ buz:state, sha: shahash });
      sha = crypto.createHash('sha256');
      c++;
    }

    if (i === buf.length - 1) {
      console.log(`${i}\t${sha.digest('base64')}`);
    }
  }
  console.log(`${c} ${(c / buf.length * 100).toFixed(2)}%`);
  return hashes;
}

var r1, r2, r3;
r1 = test(new Buffer(data.data1))
r2 = test(new Buffer(data.data2))
r3 = test(new Buffer(data.data3))

console.log(`match first: ${r1[0].sha == r2[0].sha && r2[0].sha == r3[0].sha}`);
console.log(`match last: ${r1[r1.length-1].sha == r2[r2.length-1].sha && r2[r2.length-1].sha == r3[r3.length-1].sha}`);
